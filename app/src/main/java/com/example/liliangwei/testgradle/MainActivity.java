package com.example.liliangwei.testgradle;
import com.example.liliangwei.testgradle.view.ArcMenu.OnMenuItemClickListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.gesture.GestureOverlayView;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.liliangwei.testgradle.view.ArcMenu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;

public class MainActivity extends Activity implements OnItemClickListener {

    private Context context;
    private ArcMenu mArcMenu;
    private GridView mGridView;
    private List<Map<String, Object>> dataList;
    private SimpleAdapter mSimpleAdapter;
    private int[] image = { R.drawable.address_book, R.drawable.calendar,
            R.drawable.camera, R.drawable.clock, R.drawable.games_control,
            R.drawable.messenger, R.drawable.ringtone, R.drawable.settings,
            R.drawable.speech_balloon, R.drawable.weather, R.drawable.world,
            R.drawable.youtube };

    private TextView welcomeTextView;
    private static String welcome = "";

    private String[] text = { "地图显示", "公交查询", "离线地图", "导航", "登录", "注册", "智能助手",
            "个人中心", "娱乐", "浏览器", "紧急拨号", "关于" };

    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Intent intent_to_map = new Intent(context, MapActivity.class);
                   startActivity(intent_to_map);

                    break;

            }

        };

    };


    private void initView() {
        mArcMenu = (ArcMenu) findViewById(R.id.id_right_bottom);
      //  gestureOverlayView = (GestureOverlayView) findViewById(R.id.id_gestureOverlayView);

    }

    private void initEvent() {
        mArcMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {

            public void onClick(View view, int pos) {
                Toast.makeText(MainActivity.this, pos + ":" + view.getTag(),
                        Toast.LENGTH_SHORT).show();

                final Message message = new Message();

                switch (pos) {
                    case 1:
                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(1000);
                                    message.what = 1;
                                    handler.sendMessage(message);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }

                        }.start();

                        break;

                    case 2:
                        // Intent intent_to_bus = new Intent(context,
                        // BusActivity.class);
                        // startActivity(intent_to_bus);

                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(1000);
                                    message.what = 2;
                                    handler.sendMessage(message);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }

                        }.start();

                        break;

                    case 3:
                        // Intent intent_to_download = new Intent(context,
                        // DownloadActivity.class);
                        // startActivity(intent_to_download);

                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(1000);
                                    message.what = 3;
                                    handler.sendMessage(message);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }

                        }.start();

                        break;

                    case 4:
                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(1000);
                                    message.what = 4;
                                    handler.sendMessage(message);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }

                        }.start();

                        break;

                    case 5:
                        // Intent intent_to_login = new Intent(context,
                        // LoginActivity.class);
                        // startActivity(intent_to_login);

                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(1000);
                                    message.what = 5;
                                    handler.sendMessage(message);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }

                        }.start();

                        break;

                    case 6:
                        // Intent intent_to_chat = new Intent(context,
                        // ChatActivity.class);
                        // startActivity(intent_to_chat);

                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(1000);
                                    message.what = 6;
                                    handler.sendMessage(message);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }

                        }.start();

                        break;

                }

            }
        });

    }// initEvent();

    private List<Map<String,Object>> getData()
    {
        for (int i = 0; i < image.length;i++)
        {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("image", image[i]);
            map.put("text", text[i]);
            dataList.add(map);

        }
        return dataList;
    }
    private void initGridView()
    {
        mGridView = (GridView)findViewById(R.id.id_gridview);
        mGridView.setOnItemClickListener(this);
        dataList = new ArrayList<Map<String, Object>>();
        mSimpleAdapter = new SimpleAdapter(context, getData(),
                R.layout.gridview_item, new String[]{"image", "text"},
                new int[]{R.id.id_pic, R.id.id_text});
        mGridView.setAdapter(mSimpleAdapter);

    }
    private void initNetworkConnecting()
    {
        ConnectivityManager connectManager = (ConnectivityManager)context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectManager.getActiveNetworkInfo();
        if (networkInfo != null)
        {

        }
        else
        {
            Builder builder = new Builder(context);
            builder.setTitle("网络状态");
            builder.setMessage("你当前未连接网络，请连接网络");
            builder.setPositiveButton("打开wifi", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    WifiManager wifiManager = (WifiManager) context.getSystemService(WIFI_SERVICE);
                    wifiManager.setWifiEnabled(true);
                    Toast.makeText(context, "已经为你打开wifi", Toast.LENGTH_SHORT).show();
                }
            });

            builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();

        }


    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.context = this;

        initView();
        initEvent();
        initGridView();
        initWelcome();
        initNetworkConnecting();
    }

    private void initWelcome() {
        welcomeTextView = (TextView)findViewById(R.id.id_welcome);
        Intent intent_login = getIntent();
        welcome = intent_login.getStringExtra("welcoome");

        if (null == welcome){
            welcomeTextView.setText("欢迎使用交通kkk助手");
        }
        else {
            welcomeTextView.setText(welcome+"欢kk迎使用交通助手");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        switch (position){
            case 0:
                Intent intent_map = new Intent(context,MapActivity.class);
                startActivity(intent_map);
                overridePendingTransition(R.anim.come_in, R.anim.come_out);
                break;

            case 9:

                Intent intentBrowser = new Intent(context,BrowserActivity.class);
                startActivity(intentBrowser);
                overridePendingTransition(R.anim.come_in,R.anim.come_out);
                break;
        }
    }
}
