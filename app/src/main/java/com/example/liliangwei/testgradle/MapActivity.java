package com.example.liliangwei.testgradle;

import android.animation.AnimatorSet;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.model.LatLng;
import com.example.liliangwei.testgradle.MyOrientationListener.OnOrientationListener;

//import com.baidu.location.BDLocation;
//import com.baidu.location.BDLocationListener;
//import com.baidu.location.LocationClient;
//import com.baidu.location.LocationClientOption;

import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationConfiguration;

import static android.view.View.*;
import static com.baidu.mapapi.map.MyLocationConfiguration.*;

public class MapActivity extends Activity {

    private MapView mMapView;
    private BaiduMap mBaiduMap;
    static boolean isDisplayScreen = false;


    private Context context;

    private LocationClient mLocationClient;
    private MyLocationListener mLocationListener;
    private boolean isFirstIn = true;
    private double mLatitude;
    private double mLongitude;

    private BitmapDescriptor mIconLocation;
    private MyOrientationListener myOrientationListener;
    private float mCurrentX;
    Button requestLocButton;
    BitmapDescriptor mCurrentMarker;

    private LocationMode mLocationMode;
    RadioGroup.OnCheckedChangeListener radioButtonListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SDKInitializer.initialize(getApplicationContext());

        setContentView(R.layout.activity_map);
        this.context = this;

        initView();

        initLocation();
    }

    private void initView() {
        mMapView = (MapView) findViewById(R.id.bmapView);
        mBaiduMap = mMapView.getMap();

        mBaiduMap.setMyLocationEnabled(true);


        requestLocButton = (Button)findViewById(R.id.button1);
        requestLocButton.setText("普通");



    }

    private void initLocation(){
        mLocationMode = LocationMode.NORMAL;
        OnClickListener btnClickListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (mLocationMode){
                    case NORMAL:
                        requestLocButton.setText("跟随");
                        mLocationMode = LocationMode.FOLLOWING;
                        mBaiduMap.setMyLocationConfigeration(new MyLocationConfiguration(mLocationMode,true,mCurrentMarker));
                        break;
                    case COMPASS:
                        requestLocButton.setText("普通");
                        mLocationMode = LocationMode.NORMAL;
                        mBaiduMap
                                .setMyLocationConfigeration(new MyLocationConfiguration(
                                        mLocationMode, true, mCurrentMarker));
                        break;
                    case FOLLOWING:
                        requestLocButton.setText("罗盘");
                        mLocationMode = LocationMode.COMPASS;
                        mBaiduMap
                                .setMyLocationConfigeration(new MyLocationConfiguration(
                                        mLocationMode, true, mCurrentMarker));
                        break;
                }
            }
        };

        requestLocButton.setOnClickListener(btnClickListener);
        RadioGroup group = (RadioGroup) this.findViewById(R.id.radioGroup);
        radioButtonListener = new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.defaulticon) {
                    // 传入null则，恢复默认图标
                    mCurrentMarker = null;
                    mBaiduMap
                            .setMyLocationConfigeration(new MyLocationConfiguration(
                                    mLocationMode, true, null));
                }
                if (checkedId == R.id.customicon) {
                    // 修改为自定义marker
                    mCurrentMarker = BitmapDescriptorFactory
                            .fromResource(R.drawable.ic_launcher);
                    mBaiduMap
                            .setMyLocationConfigeration(new MyLocationConfiguration(
                                    mLocationMode, true, mCurrentMarker));
                }
            }
        };
        group.setOnCheckedChangeListener(radioButtonListener);


        mLocationClient = new LocationClient(this);
        mLocationListener = new MyLocationListener();
        mLocationClient.registerLocationListener(mLocationListener);

        LocationClientOption option = new LocationClientOption();
        option.setCoorType("bd0911");
        option.setOpenGps(true);
        option.setScanSpan(1000);

        mLocationClient.setLocOption(option);

        mLocationClient.start();

    }

    @Override
    protected void onStart() {
        super.onStart();

        mBaiduMap.setMyLocationEnabled(true);
        if (!mLocationClient.isStarted()){
            mLocationClient.start();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
         mMapView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();

          mBaiduMap.setMyLocationEnabled(false);
        mLocationClient.stop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
       mMapView.onPause();
    }


    private class MyLocationListener implements BDLocationListener {
        @Override
        public void onReceiveLocation(BDLocation bdLocation) {
            MyLocationData data = new MyLocationData.Builder()
                    .direction(100)
                    .accuracy(bdLocation.getRadius())
                    .latitude(bdLocation.getLatitude())
                    .longitude(bdLocation.getLongitude())
                    .build();

            mBaiduMap.setMyLocationData(data);

            if (isFirstIn){
                LatLng latlng = new LatLng(bdLocation.getLatitude(),bdLocation.getLatitude());
                MapStatusUpdate msu = MapStatusUpdateFactory.newLatLng(latlng);
                mBaiduMap.animateMapStatus(msu);
                isFirstIn = false;

            }
        }

        @Override
        public void onReceivePoi(BDLocation bdLocation) {

        }
    }
}